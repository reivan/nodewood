const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const argv = require('yargs').argv;

const inputDir = path.join(__dirname, argv.input || './input');
const outputDir = path.join(__dirname, argv.output || './output');

const moveFiles = (inputDir, outputDir, callback) => {
  fs.readdir(inputDir, { withFileTypes: true }, (err, files) => {
    if (err) {
      console.log(err);
      return;
    }

    const filesToProcess = files.length;
    let filesProcessed = 0;

    // a way to find out when to call callback
    const onFileProcessed = err => {
      if (err) {
        console.log(err);
        return;
      }
      filesProcessed += 1;
      if (filesProcessed === filesToProcess) {
        callback();
      }
    };

    files.forEach(f => {
      const inputFile = `${inputDir}/${f.name}`;
      const outputFile = `${outputDir}/${f.name[0]}/${f.name}`;

      if (f.isDirectory()) {
        moveFiles(inputFile, outputDir, onFileProcessed);
        return;
      }

      fs.mkdir(`${outputDir}/${f.name[0]}`, () => {
        fs.copyFile(inputFile, outputFile, onFileProcessed);
      });
    });
  });
};

rimraf(outputDir, err => {
  if (err) {
    console.log(err);
    return;
  }
  fs.mkdir(outputDir, err => {
    if (err) {
      console.log(err);
      return;
    }
    moveFiles(inputDir, outputDir, () => {
      // delete input folder
      if (argv.d) {
        rimraf(inputDir, err => {
          if (err) {
            console.error('can"t remove input folder');
            return;
          }
          console.log('input removed');
        });
      }
    });
  });
});
