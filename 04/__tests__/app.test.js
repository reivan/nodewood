const request = require('supertest');
const createServer = require('../server/index.js');

describe('Test the root path', () => {
  let server = createServer(8111);

  afterEach(() => {
    server.close();
  });

  it('returns html', async () => {
    const response = await request(server).get('/');
    expect(response.text).toMatch('html');
  });

  it('serves static files', async () => {
    const response = await request(server).get('/assets/img/Music.jpg');
    expect(response.statusCode).toBe(200);
  });

  it('rejects incorrect input', async () => {
    const response = await request(server).post('/');
    expect(response.statusCode).toBe(400);
  });

  it('accepts correct input', async () => {
    const response = await request(server)
      .post('/')
      .send({
        name: 'Name',
        email: 'Email',
        message: 'Сообщение от пользователя'
      });
    expect(response.statusCode).toBe(200);
  });
});
