module.exports.index = ctx => {
  ctx.render('pages/index');
};

module.exports.login = ctx => {
  ctx.render('pages/login');
};

module.exports.admin = ctx => {
  ctx.render('pages/admin');
};

module.exports.submitMessage = async ctx => {
  const { name, email, message } = ctx.request.body;
  ctx.status = 400;

  if (name && email && message) {
    ctx.status = 200;
  }
};
