const Koa = require('koa');
const Pug = require('koa-pug');
const argv = require('yargs').argv;
const path = require('path');
const staticMiddleware = require('koa-static');

const router = require('./router');

const app = new Koa();

const hwRoot = path.join(__dirname, '..');

// eslint-disable-next-line
new Pug({
  viewPath: path.join(hwRoot, '/template'),
  noCache: true,
  app
});

app.use(staticMiddleware(path.join(hwRoot, '/static')));
app.use(router.routes());

const createServer = port => app.listen(port);

module.exports = createServer;

const port = argv.p;
if (port) {
  createServer(port);
}
